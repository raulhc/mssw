entityFactory = require '../lib/entity'
Entity = entityFactory.class
assert = require 'assert'

describe 'Entity model', ->

  it 'is born', (done)->
    entityFactory name:'my test', (err, entity)->
      assert.ok not err
      assert.ok entity instanceof Entity
      do done

  it 'has an id', (done)->
    entityFactory (err, entity)->
      assert.ok not err
      assert.ok entity.id
      do done

  it 'cant set the id', (done)->
    entityFactory {id: 1}, (err, entity)->
      assert.ok err instanceof Error
      assert.equal entity, null
      do done

  it 'cant change the id', (done)->
    entityFactory (err, entity)->
      myId = entity.id
      assert.throws -> entity.id = myId + 1
      assert.equal entity.id, myId
      do done

  it 'cant change __attributes reference', (done)->
    entityFactory (err, entity)->
      assert.throws -> entity.__attributes = {}
      do done

  it 'born with properties', (done)->
    entityFactory {name: 'Some Name', born: new Date}, (err, entity)->
      assert.ok entity instanceof Entity
      assert.equal entity.get('name'), 'Some Name'
      assert.ok entity.get('born') instanceof Date
      do done

  it 'list attribute keys', (done)->
    entityFactory {name: 'Some Name', born: new Date}, (err, entity)->
      entity.set 'cat', 'Spot'
      assert.deepEqual entity.keys, ['born', 'cat', 'id', 'name']
      do done

  it 'is born without config', (done)->
    entityFactory (err, entity)->
      assert.ok not err
      assert.ok entity instanceof Entity
      do done

  it 'adds members', (done)->
    entityFactory {}, (err, e1)->
      entityFactory {}, (err, e2)->
        entityFactory {}, (err, e3)->
          e3.addMembers e1, e2, (err)=>
            assert.ok not err
            e3.members (err, e3Members)->
              assert.deepEqual e3Members, [e1, e2]
              do done

