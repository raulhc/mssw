mssw — Message Switch
=====================

Uma infra definida e extensível por plugins para repasse de mensagens, que inicialmente irá substituir sistemas de lista de discussão.

![mssw diagram](http://raulhc.cc/pub/Projetos/Mssw/mssw-primeira-modelagem.png)

Visite: http://raulhc.cc/Projetos/Mssw
