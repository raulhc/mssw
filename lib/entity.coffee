clone = require 'clone'

# TODO: extending infra
# Most core methods was planed to call registered methods. This methods extends
# or redefines the core methods. So, they ever change its behavior.
# Also, all methods triggers one or more related events. This observers will be
# called trigging an event on this class, allowing plugins to register for all
# entities, and trigging an event on the object itself, allowing plugins to
# register only for some entities.
class Entity

  constructor: (attributes={})->
    throw new Error 'You can not set the entity id!' if attributes.id?
    # `attributes` will no more be directly accessible outside this method,
    # so the data inside it is protected by @get and @set methods.
    attributes = clone attributes # this line protects the data
    attributes.id = do Math.random
    Object.defineProperty this, '__attributes',
      get: -> attributes
      set: -> throw new Error '__attributes is a read only property.'
    Object.defineProperty this, 'id',
      get: -> attributes.id
      set: -> throw new Error 'id is a read only property.'
    @__members = {} # temporary...

  # The right public method to access entity attributes
  get: (key)->
    # TODO: call registered "get" methods
    @__attributes[key]

  # The right public method to define entity attributes
  set: (key, value)->
    @__saved = false
    # TODO: call registered "set" methods
    if key is 'id' then @id else @__attributes[key] = value

  # The right public method to list entity attributes
  Object.defineProperty @::, 'keys', get: ->
    # TODO: call registered "keys" methods
    do Object.keys(@__attributes).sort

  save: (callback)->
    # TODO: call registered beforeSave methods
    # TODO: call registered save methods
    # TODO: call registered afterSave methods
    callback null, this

  # Register one or more entities as a member of this
  addMembers: (entities..., callback=->)->
    @__members[e.id] = e for e in entities
    callback null

  members: (callback=->)->
    callback null, (e for id,e of @__members)


entityFactory = (config..., callback=->)->
  # TODO: call registered "beforeCreate" methods
  config = config[0] || {}
  try
    entity = new Entity config
  catch err
    # TODO: call registered observer "onCreateFail"
    return callback err, null
  # TODO: call registered "afterCreate" methods
  entity.save callback

entityFactory.class = Entity

module.exports = entityFactory

